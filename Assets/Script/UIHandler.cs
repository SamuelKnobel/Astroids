﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour
{

    public Text scoreText;
    public Text restartText;
    public Text startText;
    public Text gameOverlayText;
    public Text keyInfoText;
    public Image Recording;

    public Slider diffictultySlider;
    public Text DifficultyLevelValue;
    public AudioSource backgroundmusic;
    public Toggle T_MuteMusic;
    public Toggle T_MuteAll;
    public Toggle T_ActivateMouse;

    public GameController gameController;
    public LanguageHandler LanguageHandler;

    public bool showStartCountDown;

    private void Awake()
    {
        //diffictultySlider.value = diffictultySlider.minValue;

    }
    // Start is called before the first frame update
    void Start()
    {
        SetRecording();
        backgroundmusic.mute = true;
        restartText.gameObject.SetActive(false);
        startText.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (showStartCountDown)
        {
            startText.gameObject.SetActive(false);
            SetGameOverlayText((Mathf.RoundToInt(gameController.startCountDown.RestTime)).ToString());
            if (gameController.startCountDown.RestTime<0)
            {
                SetGameOverlayText("");
                showStartCountDown = false;
                
            }
        }
        if (Input.GetKey(KeyCode.LeftControl)& Input.GetKeyDown(KeyCode.M))
        {
            T_ActivateMouse.isOn = !T_ActivateMouse.isOn;
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            GameController.isRecording = !GameController.isRecording;
            SetRecording();
            gameController.LogHandler.writeToLog("Recording = " + GameController.isRecording);
        }
    }
    public void SetGameOverlayText(string txt)
    {
        ChangeTextColor(gameOverlayText);
        gameOverlayText.text = txt;
    }

    public void ChangeTextColor(Text textfield)
    {
        textfield.color = gameController.levelHandler.currentLevel.textColor;
    }


    public void UpdateScore(int score)
    {
        scoreText.text = score.ToString();
    }
    public void ShowStats(int numberOfTargetHits, int totalTarges, int numberOfDistractor, int totalDistractors)
    {
        string txt= "Targets: " + numberOfTargetHits +
            " / " + totalTarges + "\n" + "Distractors: " + numberOfDistractor +
            " / " + totalDistractors;

        SetGameOverlayText(txt);
        restartText.gameObject.SetActive(true);
        if (!gameController.LogHandler.LevelStatsWritten)
        {
            gameController.LogHandler.writePositionToFile();
            gameController.LogHandler.writeToLog("Level:" + gameController.levelHandler.lastLevel.level);
            gameController.LogHandler.CalculateHitrate(numberOfTargetHits, totalTarges, "Targets");
            gameController.LogHandler.CalculateHitrate(numberOfDistractor, totalDistractors, "Distractors");
            gameController.LogHandler.CalculateAccuracy(numberOfTargetHits);
            gameController.LogHandler.LevelStatsWritten = true;
        }
    }

    public void ChangeDifficutly()
    {
        gameController.levelHandler.OnLevelChange((int)diffictultySlider.value);

        foreach (Mover astroidMover in FindObjectsOfType<Mover>())
        {
            astroidMover.UpdateSpeed(gameController.levelHandler.currentLevel.speed);
        }
        DifficultyLevelValue.text = ((int)diffictultySlider.value + 1).ToString();
        SetGameOverlayText(gameOverlayText.text);
        if (!gameController.restart)
        {
            gameController.levelHandler.ResetStats();
        }

    }

    public void ChagneVulnerability(Toggle toggle)
    {
        if (toggle.isOn)
            GameController.invulnerable = true;
        else
            GameController.invulnerable = false;
    }

    public void MuteMusic(Toggle toggle)
    {
        if (toggle.isOn)
            backgroundmusic.mute = true;
        else
            backgroundmusic.mute = false;
    }   
    public void MuteAll(Toggle toggle)
    {
        if (toggle.isOn)
        {
            T_MuteMusic.isOn = true;
            AudioListener.volume = 0;
            T_MuteMusic.interactable = false;
        }
        else
        {
            AudioListener.volume = 1;
            T_MuteMusic.interactable = true;

        }
    }

    public void ActivateMouse(Toggle toggle)
    {
        if (toggle.isOn)
        {
            //T_ActivateMouse.isOn = true;
            gameController.MouseInputActive = true;
        }
        else
        {
            gameController.MouseInputActive = false;
        }
    }

    public void SetRecording()
    {
        if (GameController.isRecording)
        {
            Recording.color = Color.red;
        }
        else
            Recording.color = Color.white;
    }
}
