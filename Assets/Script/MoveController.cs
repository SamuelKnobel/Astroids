﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    public int ScreenWidth;
    public int ScreenHeight;

    GameController gameController;

    void Awake()
    {
        ScreenWidth = Screen.width;
        ScreenHeight = Screen.height;
    }

    Vector3 PlayerPosition;
    // Update is called once per frame
    void Update()
    {
        if (gameController== null)
        {
            gameController = FindObjectOfType<GameController>();
            return;
        }

        if (gameController.MouseInputActive)
        {
            PlayerPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            PlayerPosition.x = Mathf.Clamp(PlayerPosition.x, -6f, 6f);
            PlayerPosition.y = 0;
            PlayerPosition.z = 0;
            gameObject.transform.position = PlayerPosition;
        }
    }
}
