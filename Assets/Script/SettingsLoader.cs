﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class SettingsLoader : MonoBehaviour
{
    [SerializeField] Languages Languages;
    [SerializeField] int StartingLevel;

    const string SETTINGSFILENAME = "Settings.txt";
    [SerializeField] string SettingsFilePath;
    [SerializeField] string SettingsText="";
    [SerializeField] GameController gameController;

    private void Awake()
    {
        SettingsFilePath = Path.Combine(Application.streamingAssetsPath, SETTINGSFILENAME);
        StreamReader reader = new StreamReader(SettingsFilePath);
        SettingsText= reader.ReadToEnd();
        SettingParser();

    }

    // Start is called before the first frame update
    void Start()
    {
        gameController.uIHandler.diffictultySlider.value = StartingLevel-1;
        gameController.uIHandler.ChangeDifficutly();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SettingParser()
    {
        string[] lines = SettingsText.Split('\n');
        int count = 0;
        foreach (string line in lines)
        {
            count++;
            if (line.Length < 2  || line.StartsWith("#") || line.StartsWith(" "))
                continue;


            string[] lineparts = line.Split('=');
            string SettingsName= lineparts[0].Trim();
            string SettingsValue = lineparts[1].Trim();
            switch (SettingsName)
            {                
                case "Language":

                    if (!Enum.TryParse(SettingsValue, out Languages))
                    {
                        Debug.LogError("Language not Recognized, Valid Inputs are 'German' and 'English' ");
                        Languages = Languages.NotDefined;
                    }
                    else
                    {
                        Languages = (Languages)Enum.Parse(typeof(Languages), SettingsValue);
                    }
                    break; 
                    
                case "SkipMenu":
                    Debug.LogWarning("To Be Implemented");
                    break;
                              
                case "StartingLevel":
                    if (!int.TryParse(SettingsValue, out StartingLevel))
                    {
                        Debug.LogError("Number for Starting Level not Recognized, Valid Inputs are 1-10 ");
                        StartingLevel = 1;
                    }
                    else
                    {
                        StartingLevel = int.Parse(SettingsValue);
                    }
                    if (StartingLevel <1)
                    {
                        Debug.LogError("Error in Settings, Startinglevel cannot be lower than 0");
                        StartingLevel = 1;
                    }
                    if (StartingLevel >10)
                    {
                        Debug.LogError("Error in Settings, Startinglevel cannot be higher than 10");
                        StartingLevel = 10;
                    }
                    break;

                default:
                    Debug.LogWarning("Unused Settings:");
                    Debug.LogWarning(line);
                    break;
            }
        }
    }
}




public enum Languages
{
    NotDefined, German, English
}
