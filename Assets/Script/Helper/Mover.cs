﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    public float speed;
    private Rigidbody rb;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        //if (CompareTag("Bolt"))
            rb.velocity = transform.forward * speed;
        //else
        //    rb.velocity = transform.forward * speed;

    }
    public void UpdateSpeed(float newSpeed)
    {
        speed = newSpeed;
        //if (CompareTag("Bolt"))
            rb.velocity = Vector3.forward * speed;
        //else
        //    rb.velocity = Vector3.forward * speed;
    }
}
