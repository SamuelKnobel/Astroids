﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shootable : MonoBehaviour
{
    bool hit;
    private void Awake()
    {
        hit = false;
    }

    public void AddToShotList()
    {
        if (!hit)
        {
            if (CompareTag("Target"))
            {
                GameController.NumberOfTargetHits++;
            }
            if (CompareTag("Distractor"))
            {
                GameController.NumberOfDistractorHits++;
            }
            hit = true;
        }
    }
}
