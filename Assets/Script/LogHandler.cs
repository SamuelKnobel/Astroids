﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class LogHandler : MonoBehaviour
{

    public GameController GameController;
    public bool LevelStatsWritten;
    public string pathToSaveFolder;

    public string pathToSaveFile_Stats;
    public string nameSaveFile_Stats;    
    public string pathToSaveFile_Mouse;
    public string nameSaveFile_Mouse;

    public List<string> ShipPosition;

    public string textToWrite;
    public float StartTimeProgramm;
    public float StartRecord;

    public static double currentTimeStamp;

    public int clickCounter;
    public float Hitrate;
    public float Accuracy;

    // Start is called before the first frame update
    void Awake()
    {
        ShipPosition = new List<string>();
        currentTimeStamp = ConvertToTimestamp(DateTime.UtcNow);
        pathToSaveFolder = Application.streamingAssetsPath + "/Output/";

        if (!Directory.Exists(pathToSaveFolder))
        {
            Directory.CreateDirectory(pathToSaveFolder);
        }
        else
        {
            print("Directory Exists");
        }
        clickCounter = 0;
        StartTimeProgramm =(float)ConvertToTimestamp(DateTime.UtcNow);
        CreateSaveFiles();

    }
    private void Start()
    {
        writeToLog("Programm Started = " + Mathf.RoundToInt(StartTimeProgramm));
    }


    private void FixedUpdate()
    {
        currentTimeStamp = ConvertToTimestamp(DateTime.UtcNow);
        if (GameController.gameStarted & ! GameController.showStats)
        {

            GameObject player = GameObject.FindGameObjectWithTag("Player");
            string stringToAdd = currentTimeStamp+":"+ player.transform.position.x.ToString();
            ShipPosition.Add(stringToAdd);
        }
    }

    public void writeToLog(string textToAdd)
    {
        if (LevelStatsWritten)
            return;

        textToWrite = currentTimeStamp.ToString() + ": " + textToAdd;

        if (textToWrite.Length == 0)
        {
            Debug.LogWarning("Nothing To Write");
            return;
        }
        StreamWriter writer = new StreamWriter(pathToSaveFile_Stats, true);
        writer.WriteLine(textToWrite);
        textToWrite = "";
        writer.Close();
    }

    public void writePositionToFile()
    {
        StreamWriter writer = new StreamWriter(pathToSaveFile_Mouse, true);
        foreach (string item in ShipPosition)
        {
            writer.WriteLine(item);
        }
        writer.Close();
        ShipPosition = new List<string>();

    }

    public void AddClick()
    {
        clickCounter++;
    }
   
    public void CalculateHitrate(float HitTargets, float TotalNumberOfTargets, string type)
    {
        Hitrate = (float)HitTargets / TotalNumberOfTargets;
        print(HitTargets);
        print(TotalNumberOfTargets);
        writeToLog(type + " Hitrate = " + Hitrate);
    }

    public void CalculateAccuracy(float HitTargets)
    {
        print(clickCounter);
        print(HitTargets);
        Accuracy = (float)HitTargets / clickCounter;
        writeToLog("Accuracy = " + Accuracy);
    }
    public void CreateSaveFiles()
    {

        nameSaveFile_Stats = "Stats_" + Mathf.RoundToInt(StartTimeProgramm) + ".txt";
        pathToSaveFile_Stats = pathToSaveFolder + "/" + nameSaveFile_Stats;

        if (!File.Exists(pathToSaveFile_Stats))
            File.Create(pathToSaveFile_Stats);

        nameSaveFile_Mouse = "Mouse_" + Mathf.RoundToInt(StartTimeProgramm) + ".txt";
        pathToSaveFile_Mouse = pathToSaveFolder + "/" + nameSaveFile_Mouse;

        if (!File.Exists(pathToSaveFile_Mouse))
            File.Create(pathToSaveFile_Mouse);
    }
    public static double ConvertToTimestamp(DateTime value)
    {
        TimeSpan span = (value - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));
        return (double)span.TotalSeconds;
    }

}
