﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour {

    // Script References
    public LevelHandler levelHandler;
    public UIHandler uIHandler;
    public SettingsLoader settingsLoader;
    public LogHandler LogHandler;

    public bool gameStarted;
    public bool startSpawning;

    public bool MouseInputActive;

    public List<GameObject> SpawnPool;
    public GameObject TargetHolder;

    public Timer startCountDown;
    public Timer spawnTimer;

    public Vector3 spawnValues;

    public static bool invulnerable;
    public static bool isRecording;

    public bool restart;
    public int score;
    public int spawnCount;
    public int TargetsPerRound;
    public int DistractorsPerRound;

    public static int NumberOfTargetHits = 0;
    public static int NumberOfDistractorHits = 0;
    public bool showStats;

    public void Awake()
    {
        Initialize();
    }
    private void Initialize()
    {
        isRecording = false;
        spawnCount = 0;
        showStats = false;
        NumberOfTargetHits = 0;
        NumberOfDistractorHits = 0;
        gameStarted = false;
        spawnValues = new Vector3(6, 0, 16);
        invulnerable = true;
        restart = false;
        score = 0;
        uIHandler.UpdateScore(score);

        startCountDown.description = "StartCountDown";
        startCountDown.Duration = 3;

        spawnTimer.description = "SpawnTimer";


    }

    private void Update()
    {
        if (!gameStarted)
        {
            if (Input.GetKeyDown(KeyCode.Space) || (MouseInputActive & Input.GetMouseButtonDown(0)))
            {
                LogHandler.clickCounter = -1;

                LogHandler.LevelStatsWritten = false;
                LogHandler.writeToLog("Level Start: " + levelHandler.currentLevel.level);

                showStats = false;
                gameStarted = true;
                startCountDown.Run();
                uIHandler.showStartCountDown = true;
            }
        }

        if (startCountDown.Finished & startCountDown.isActive)
        {
            NumberOfDistractorHits = 0;
            NumberOfTargetHits = 0;
            startCountDown.isActive = false;
            startSpawning = true;
            spawnTimer.Duration = 0.1f;
            spawnTimer.Run();
        }
        if (startSpawning)
        {
            SpawnElements();
        }
        if (!startSpawning & showStats)
        {
            restart = true;
            levelHandler.CalculateNextLevel();
            uIHandler.ShowStats(NumberOfTargetHits, levelHandler.lastLevel.numberOfTargets,
                NumberOfDistractorHits, levelHandler.lastLevel.numberOfDistractors);
        }
        
        Restart();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    void Restart()
    {
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.Space) || (MouseInputActive & Input.GetMouseButtonDown(0)))
            {
                print("Restart");
                Initialize();

                LogHandler.clickCounter = -1;

                GeneratePoolOfElementsToSpawn();
                showStats = false;
                startCountDown.Run();
                uIHandler.showStartCountDown = true;
                levelHandler.nextLevelDefined = false;
                uIHandler.restartText.gameObject.SetActive(false);
                levelHandler.OnLevelChange((int)uIHandler.diffictultySlider.value);
                levelHandler.ResetStats();
                gameStarted = true;
                LogHandler.LevelStatsWritten = false;
                LogHandler.writeToLog("Level Start: " + levelHandler.currentLevel.level);

            }
        }
    }

    public void GeneratePoolOfElementsToSpawn()
    {
        foreach (Transform child in TargetHolder.transform)
        {
            Destroy(child.gameObject);
        }
        SpawnPool = new List<GameObject>();

        for (int i = 0; i < levelHandler.NumberOfElements.x + levelHandler.NumberOfElements.y; i++)
        {
            GameObject Target;
            if (i < levelHandler.NumberOfElements.x)
            {
                Target = levelHandler.GetRandomTarget();
            }
            else
                Target = levelHandler.GetRandomDistractor();

            GameObject go = Instantiate(Target,TargetHolder.transform);
            go.SetActive(false);
            SpawnPool.Add(go);       
        }
        SpawnPool.Shuffle();
    }

    public void SpawnElements()
    {
        if (SpawnPool.Count > 0 )
        {
            if (spawnTimer.Finished)
            {
                GameObject Target = SpawnPool[0];
                SpawnPool.RemoveAt(0);
                Vector3 spawnposition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnrotation = Quaternion.identity;
                Target.transform.position = spawnposition;
                Target.transform.rotation = spawnrotation;
                Target.SetActive(true);
                Target.GetComponent<Mover>().speed = levelHandler.currentLevel.speed;
                spawnCount++;
                spawnTimer.Duration = levelHandler.currentLevel.spawnFrequency;
                spawnTimer.Run();
            }   
        }
        else
        {
            if (GameObject.FindGameObjectsWithTag("Target").Length > 0)
                return;
            if (GameObject.FindGameObjectsWithTag("Distractor").Length > 0)
                return;

            startSpawning = false;
            showStats = true;
        }
    }




    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        uIHandler.UpdateScore(score);
    }
    public void SetScore(int newScoreValue)
    {
        score = newScoreValue;
        uIHandler.UpdateScore(score);
    }    
}

public static class IListExtensions
{
    /// <summary>
    /// Shuffles the element order of the specified list.
    /// </summary>
    public static void Shuffle<T>(this IList<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }
}