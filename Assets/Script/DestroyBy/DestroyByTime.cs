﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Used to Remove the explosions
public class DestroyByTime : MonoBehaviour {
    public float lifetime;

    private void Start()
    {
        lifetime = 2.5f;
        Destroy(gameObject, lifetime);
    }


}
