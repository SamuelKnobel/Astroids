﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject PlayerExplosion;
    public int scoreValue;

    private GameController gameController;

    private void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot finde Gamecontroller Script");
        }
    }


    void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Boundary"))
            return;
        if (other.CompareTag("Bolt"))
        {
            GetComponent<Shootable>().AddToShotList();
        }

        if (other.CompareTag("Player"))
        {
            if (GameController.invulnerable)
            {
                return;
            }

            Instantiate(PlayerExplosion, other.transform.position, other.transform.rotation);
            //gameController.GameOver();
        }
        Instantiate(explosion, transform.position, transform.rotation);
        gameController.AddScore(scoreValue);
        Destroy(other.gameObject);
        Destroy(gameObject);

    }


}
