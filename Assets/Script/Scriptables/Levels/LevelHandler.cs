﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelHandler : MonoBehaviour
{
    public GameController gameController;
    public MeshRenderer backgroundRenderer;
    public GameObject starField;
    [SerializeField] DifficultyLevel _currentLevel;
    public DifficultyLevel currentLevel
    {
        get
        {
            if (_currentLevel == null)
                return defaultLevel;
            else
                return _currentLevel;
        }
        set
        {
            _currentLevel = value;
        }
    }
    public DifficultyLevel lastLevel;

    [SerializeField] DifficultyLevel[] levels;
    public DifficultyLevel defaultLevel;

    public bool nextLevelDefined;
    public enum LevelChange
    {
        up, 
        stay,
        down 
    }
    public LevelChange ChangeNextLevel;


    public Vector2 NumberOfElements
    {
        get
        {
            if (currentLevel == null)
            {
                return Vector2.zero;
            }
            else
            {
                return new Vector2(currentLevel.numberOfTargets, currentLevel.numberOfDistractors);
            }
        }
    }
    public GameObject GetRandomTarget()
    {
        if (currentLevel == null)
        {
            Debug.LogError("No Targets defined");
            return null;
        }
        else
        {
           return currentLevel.targets[Random.Range(0, currentLevel.targets.Length)];
        }
    }
    public GameObject GetRandomDistractor()
    {
        if (currentLevel == null)
        {
            Debug.LogError("No Distractor defined");
            return null;
        }
        else
        {
            return currentLevel.distractors[0];
        }
    }


    void Start()
    {
        //gameController.uIHandler.ChangeDifficutly();
    }
    private void Update()
    {
        
    }

    public void OnLevelChange(int newLevel)
    {
        currentLevel = GetLevel(newLevel);
        backgroundRenderer.material = currentLevel.background;
        starField.SetActive(currentLevel.starfieldActive);
        gameController.spawnTimer.Duration = currentLevel.spawnFrequency;
        gameController.GeneratePoolOfElementsToSpawn();

    }
    public void ResetStats()
    {
        gameController.SetScore(0);
        GameController.NumberOfDistractorHits = 0;
        GameController.NumberOfTargetHits = 0;
    }

    public void CalculateNextLevel()
    {
        if (!nextLevelDefined)
        {
            lastLevel = currentLevel;
            float percentHit = 0;
            float percentFalse = 0;
            if (lastLevel.numberOfTargets > 0)
                percentHit = GameController.NumberOfTargetHits / lastLevel.numberOfTargets;
            if (lastLevel.numberOfDistractors > 0)
                percentFalse = GameController.NumberOfDistractorHits / lastLevel.numberOfDistractors;

            int _currentLevel = (int)gameController.uIHandler.diffictultySlider.value;
            if (percentHit > 0.75 & percentFalse < 0.25)
            {
                _currentLevel++;
            }
            else if (percentHit < 0.5 | percentFalse > 0.75)
            {
                _currentLevel--;
            }
            else
            {
                print(percentHit);
                print(percentFalse);
            }
            _currentLevel = Mathf.Clamp(_currentLevel, 0, levels.Length - 1);
            gameController.uIHandler.diffictultySlider.value = _currentLevel;
            nextLevelDefined = true;
        }
    }




    public DifficultyLevel GetLevel(int level)
    {        
        if (level < levels.Length)
        {
            return levels[level];
        }
        else
        {
            print("Error, level cannot be loaded, default level is loaded");
            return defaultLevel;

        }
    }
}
