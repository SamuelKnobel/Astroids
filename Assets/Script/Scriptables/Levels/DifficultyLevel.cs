﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Difficulty Level")]
public class DifficultyLevel  : ScriptableObject
{   
    public int level;
    public int totalGameTime;
    public Material background;
    public GameObject[] targets;
    public GameObject[] distractors;
    public int numberOfTargets;
    public int numberOfDistractors;
    public float speed;
    public float spawnFrequency;
    public bool starfieldActive;
    public Color textColor;
}
