﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageHandler : MonoBehaviour
{
    public GameController gameController;
    public UIHandler UIHandler;
    [SerializeField] Language[] Languages;
    public Language defaultLanguage;
    public Language currentLanguage;

    public Text T_invulnerable;
    public Text T_mouseControl;
    public Text T_muteBackground;
    public Text T_muteAll;

    public Text T_DifficultyLevel;
    public Text T_GameOverlay;
    public Text T_InfoKey;
    public Text T_Score;
    public Text T_Restart;
    public Text T_Start;

    public void Awake()
    {
        currentLanguage = defaultLanguage;
    }

    // Start is called before the first frame update
    void Start()
    {
        OnLanguageChange();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            currentLanguage = Languages[0];
            OnLanguageChange();
        } 
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            currentLanguage = Languages[1];
            OnLanguageChange();
        }
    }
    public void OnLanguageChange()
    {
        T_invulnerable.text = currentLanguage.invulnerable;
        T_mouseControl.text = currentLanguage.mouseControl;
        T_muteBackground.text = currentLanguage.muteBackground;
        T_muteAll.text = currentLanguage.muteAll;
        T_InfoKey.text = currentLanguage.keyInfo.Replace("/n", "\n");
        T_Score.text = currentLanguage.score;
        T_Restart.text = currentLanguage.restart;
        T_DifficultyLevel.text = currentLanguage.difficultLevel;
        T_Start.text = currentLanguage.start;
        if (UIHandler != null)
        {
            UIHandler.ChangeTextColor(T_Restart);
            UIHandler.ChangeTextColor(T_Start);
        }
    }
}
