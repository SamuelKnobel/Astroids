﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Language")]

public class Language : ScriptableObject
{
    //Toggle Descriptions
    public string invulnerable;
    public string mouseControl;
    public string muteBackground;
    public string muteAll;

    //Info Field Texts
    public string score;
    public string start;
    public string restart;
    public string keyInfo;
    public string difficultLevel;
}
