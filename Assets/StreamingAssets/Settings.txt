# Settings File for Astroids 			#
# --------------------------------------------- #
# Only the lines without a Hashtag are   	#
# read in.                               	#
# --------------------------------------------- #

# Choose Language for the text in the    	#
# Game (German, English)     			#
Language = German	

# Define if in the beginning a Menu is shown	#
# where the user can enter the name of the 	#
# Player and choose the starting level		#
SkipMenu = false


# Default Starting Level			#
StartingLevel = 1			
